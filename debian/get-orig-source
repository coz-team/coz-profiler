#!/bin/sh
# Make tarball for Debian.  Remove irrelevant parts with unclear
# copyright status and licenses to reduce the amount of files we need
# to track the copyright status of.

set -e

targetdir=`pwd`
gitbranch=master
workdir=$(mktemp -d)
sdir=coz-profiler-upstream

cd $workdir

# Fetch source using git
git clone -b ${gitbranch} https://github.com/plasma-umass/coz ${sdir}
git clone -b gh-pages https://github.com/plasma-umass/coz ${sdir}/viewer

# Fetch third party header library
make -C ${sdir} deps/ccutil

# Derive tarball version from time of latest git commit before removing git traces
timestamp=$(cd ${sdir}; git log --date=format:%Y%m%dT%H%M --max-count=1 | \
		awk '/Date:/ { print $2}')
VER=0.0.git.$timestamp
gittag=$(cd ${sdir}; git log --max-count=1 | awk '/^commit / { print $2}')

# Remove git traces
rm -rf ${sdir}/.git*
rm -rf ${sdir}/viewer/.git*
rm -rf ${sdir}/deps/*/.git*

mv ${sdir} coz-profiler-${VER}

tar zcf $targetdir/coz-profiler-${VER}.tar.gz coz-profiler-${VER}

rm -rf $workdir

echo "Import source into packaging git repository using the following command:"
echo
echo "  gbp import-orig --upstream-vcs-tag=$gittag --pristine-tar coz-profiler-${VER}.tar.gz"
