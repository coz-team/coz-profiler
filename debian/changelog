coz-profiler (0.2.2-3) unstable; urgency=medium

  * QA upload.

  * Drop MIA maintainer Lluís Vilanova and orphan package.  Many
    thanks for all his good work.
  * Replaced obsolete build dependency pkg-config with pkgconf.
  * Update watch file format version to 4.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.7.0, no changes needed.
  * Added override_dh_clean build rule to make build rebuildable
    (Closes: #1044911).
  * Moved git repository to salsa Debian group.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 09 May 2024 11:08:30 +0200

coz-profiler (0.2.2-2) unstable; urgency=medium

  [ Lluis Vilanova ]
  * Adjusted autopkgtest checks to not fail when the kernel perf access
    block them from running.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 05 Sep 2020 11:17:25 +0200

coz-profiler (0.2.2-1) unstable; urgency=medium

  [ Lluis Vilanova ]
  * New upstream version.
  * Depend only on Python3 packages (Closes: #936336).
  * Adapt patches to new upstream version
  * Update debhelper compatibility.
  * Remove testsuite in debian/control.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 02 Sep 2020 18:53:33 +0200

coz-profiler (0.1.0-2) unstable; urgency=medium

  * Quiet down build rules a bit.
  * Adjust build to avoid underlinking when linker uses --as-needed
    (Closes: #854031).

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 01 Jul 2017 22:32:14 +0000

coz-profiler (0.1.0-1) unstable; urgency=low

  [ Petter Reinholdtsen ]
  * Started on upstream/metadata file.

  [ Lluís Vilanova ]
  * Add watch file for upstream releases
  * Change the package section into 'devel' (Closes: #848397).

  [ Petter Reinholdtsen ]
  * Changed d/rules to only run test suite when kernel
    perf_event_paranoid value is 2 or less.  (Closes: #844633)
  * Changed to use libjs-d3 and libjs-d3-tip from Debian instead of
    embedded copies.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 20 Dec 2016 21:45:34 +0000

coz-profiler (0.0.git.20161130T1802-2) unstable; urgency=medium

  [ Lluís Vilanova ]
  * Reverted 36c9b5e to remove useless 04-make-check patch
  * Added 06-viewer-install.patch to align viewer makefile with
    common.mk (Closes: #846538).
  * Added 07-coz-plot.patch to hint users how to use the viewer

  [ Petter Reinholdtsen ]
  * Adjusted 07-coz-plot.patch to suggest opening HTML page using xdg-open.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 03 Dec 2016 08:58:39 +0000

coz-profiler (0.0.git.20161130T1802-1) unstable; urgency=medium

  [ Lluís Vilanova ]
  * Fetched new upstream release from upstream git.
  * Adjusted 01-system-libelfin.patch, part of it is now upstream.
  * Removed 02-portable-spinlock.patch now included upstream.
  * Added 02-common-pwd.patch to fix use of PWD in common.mk.
  * Added 03-viewer-build.patch to drop apparenly redundant npm call
    during build.
  * Added 04-make-check.patch to make sure test binary have debug
    information.

  [ Petter Reinholdtsen ]
  * Added 05-revert-in-scope-error.patch to revert upstream commit
    breaking self test.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 30 Nov 2016 21:15:35 +0000

coz-profiler (0.0.git.20161011T1320-3) unstable; urgency=medium

  * Change architecture from any to linux-any, as the code depend on
    internal Linux perf syscalls (Closes: #844024).
  * Added 02-portable-spinlock.patch from upstream to make spinlock
    implementation portable (Closes: #844017).
  * Adjusted autopkgtest setup to allow stderr output.
  * Added new autopkgtest script test-coz-run-simple for testing a
    multithreaded program with debug info.
  * Build-depend on libelfin-dev (>= 0.2) to ensure we link using the
    new SONAME.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 16 Nov 2016 08:22:09 +0000

coz-profiler (0.0.git.20161011T1320-2) unstable; urgency=medium

  * Corrected Vcs links in d/control.
  * Add missing build dependency pkg-config. (Closes: #844015)
  * Added missing python-docutils build dependency to makae rst2man available.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 12 Nov 2016 07:57:22 +0000

coz-profiler (0.0.git.20161011T1320-1) unstable; urgency=medium

  * Initial upload (Closes: #830708).
    - Include both master and gh-pages branch from upstream.
  * Changed package to use jquery and awesome font from Debian instead
    of embedding them.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 31 Oct 2016 11:22:28 +0000
